const express = require('express');
const server = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const { yesAuth, noAuth } = require('../config/login');

const User = require('../models/users');

server.get('/d(ashboard)?', yesAuth, (req, res) => {
    res.render('../views/reg/dashboard.ejs', {
        name: req.user.name,
        email: req.user.email
    });
})

server.get('/r(eg)?', noAuth, (req, res) => {
    res.render('../views/reg/reg.ejs');
})

server.get('/a(uth)?', noAuth, (req, res) => {
    res.render('../views/reg/auth.ejs');
})


server.post('/r', (req, res) => {
    const { name, email, number, password, password2} = req.body;
    let errors = [];

    if(!name || !email || !number || !password || !password2) {
        errors.push({msg: 'გთხოვთ შეავსოთ ყველა ველი'});
    }

    if(password !== password2) {
        errors.push({msg: 'პაროლი არემთხვევა'});
    }

    if(password.length < 6) {
        errors.push({msg: 'პაროლი უნდა შეიცავდეს მინ. 6 სიმბოლოს'});
    }

    if(errors.length > 0) {
        res.render('../views/reg/reg.ejs', {
            errors,
            name,
            email,
            number,
            password,
            password2
        });
    } else {
        User.findOne({ email: email })
         .then(user => {
             if(user) {
                errors.push({msg: 'ესეთი მაილი უკვე არსებობს'});
                res.render('../views/reg/reg.ejs', {
                    errors,
                    name,
                    email,
                    number,
                    password,
                    password2
                });
             } else {
                 const newuser = new User({
                     name,
                     email,
                     number,
                     password
                 });

                bcrypt.genSalt(10, (err, salt) =>
                  bcrypt.hash(newuser.password, salt, (err, hash) => {
                      if(err) throw err;

                      newuser.password = hash;

                      newuser.save()
                       .then(user => {
                           req.flash('success_msg', 'თქვენ წარმატებით გაიარეთ რეგისტრაცია')
                           res.redirect('/user/a');
                       })
                       .catch(err => console.log(err));
                })) 
            }
        });
    }
});

server.post('/a', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/user/d',
        failureRedirect: '/user/a',
        failureFlash: true
    })(req, res, next);
});

server.get('/l(ogout)?', (req, res) => {
    req.logOut();
    req.flash('success_msg', 'თქვენ წარმატებით დატოვეთ ანგარიში');
    res.redirect('/user/a');
});

module.exports = server;