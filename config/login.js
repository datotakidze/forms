module.exports = {
    yesAuth: async function(req, res, next) {
        if (await req.isAuthenticated()) {
            return next();
        }
        req.flash('error_msg', 'გთხოვთ გაიაროთ ავტორიზაცია');
        res.redirect('/user/a');
    },
    noAuth: async function(req, res, next) {
        if (!req.isAuthenticated()) {
          return next();
        }
        res.redirect('/user/d');      
    }
}
