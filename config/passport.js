const LocalStrategy = require('passport-local').Strategy;
const bcyrpt = require('bcryptjs');
const User = require('../models/users');

module.exports = function(passport) {
    passport.use(
        new LocalStrategy({ usernameField: 'email'}, (email, password, done) => {
            User.findOne({email:email})
             .then(user => {
                 if(!user) {
                     return done(null, false, { message: 'ასეთი ემაილი არ არსებობს'})
                 }

                bcyrpt.compare(password, user.password, (err, isMath) => {
                    if(err) throw err;

                    if(isMath) {
                        return done(null, user);
                    } else {
                        return done(null, false, { message: 'პაროლი ან მომხმარებელი არასწორია'})
                    }
                })
             })
             .catch(err => console.log(err));
        })
    );


    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        })
    });
}